package ba.hk.meetme.api;


import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import ba.hk.meetme.models.GsonAdaptersModels;

public class GsonProvider {
    private static Gson mGson;

    public static Gson get() {
        if (mGson == null) {
            mGson = new GsonBuilder()
                    .registerTypeAdapterFactory(new GsonAdaptersModels())
                    .create();
        }

        return mGson;
    }
}
