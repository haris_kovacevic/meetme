package ba.hk.meetme.api;


import android.support.annotation.Nullable;

import com.fernandocejas.arrow.optional.Optional;

import org.immutables.value.Value;

import ba.hk.meetme.models.Error;

@Value.Immutable
public abstract class ApiResult<T> {

    public abstract boolean successful();

    @Nullable
    public abstract T data();

    @Nullable
    public abstract String message();

    @Nullable
    public abstract Error error();

    @Value.Derived
    public Optional<T> dataOpt() {
        return Optional.fromNullable(data());
    }

    @Value.Derived
    public Optional<String> messageOpt() {
        return Optional.fromNullable(message());
    }

    @Value.Derived
    public Optional<Error> errorOpt() {
        return Optional.fromNullable(error());
    }


}
