package ba.hk.meetme.api;


import java.util.List;
import java.util.concurrent.TimeUnit;

import ba.hk.meetme.models.Error;
import ba.hk.meetme.models.ImmutableError;
import ba.hk.meetme.models.ImmutableLoginForm;
import ba.hk.meetme.models.ImmutableRegistrationForm;
import ba.hk.meetme.models.Feed;
import ba.hk.meetme.models.Token;
import io.reactivex.Single;
import io.reactivex.schedulers.Schedulers;
import okhttp3.OkHttpClient;
import retrofit2.Retrofit;
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory;
import retrofit2.converter.gson.GsonConverterFactory;

public class NetworkApi {
    private NetworkService mService;

    private static class Loader {
        static NetworkApi INSTANCE = new NetworkApi();
    }

    public static NetworkApi getInstance() {
        return Loader.INSTANCE;
    }

    private NetworkApi() {
        mService = createRetrofit().create(NetworkService.class);
    }

    public static NetworkService getService() {
        return getInstance().mService;
    }

    private Retrofit createRetrofit() {

        OkHttpClient client = new OkHttpClient.Builder()
                .addInterceptor(new AuthenticationInterceptor())
                .readTimeout(20, TimeUnit.SECONDS)
                .build();

        return new Retrofit.Builder()
                .client(client)
                .addCallAdapterFactory(RxJava2CallAdapterFactory.createWithScheduler(Schedulers.io()))
                .addConverterFactory(GsonConverterFactory.create(GsonProvider.get()))
                .baseUrl(NetworkService.BASE_URL)
                .build();
    }


    public Single<ApiResult<Token>> login(String email, String password) {
        return mService.login(ImmutableLoginForm.builder().email(email).password(password).build()).map(result -> {
            boolean error = result.isError() || !result.response().isSuccessful() || !result.response().body().isSuccessful();
            return ImmutableApiResult.<Token>builder()
                    .successful(!error)
                    .data(!result.isError() ? result.response().body().data() : null)
                    .error(!result.isError() ? result.response().body().error() : getNetworkError())
                    .build();
        });
    }


    public Single<ApiResult<Token>> register(String email, String password) {
        return mService.register(ImmutableRegistrationForm.builder().email(email).password(password).build()).map(result -> {
            boolean error = result.isError() || !result.response().isSuccessful() || !result.response().body().isSuccessful();
            return ImmutableApiResult.<Token>builder()
                    .successful(!error)
                    .data(!result.isError() ? result.response().body().data() : null)
                    .error(!result.isError() ? result.response().body().error() : getNetworkError())
                    .build();
        });
    }


    public Single<ApiResult<List<Feed>>> getFeeds(int page) {
        return MockFeedsData.mockApi();
    }


    private Error getNetworkError() {
        return ImmutableError.builder()
                .httpCode(0)
                .message("No internet connection")
                .name("NETWORK_CONNECTION_UNAVAILABLE").build();
    }
}


