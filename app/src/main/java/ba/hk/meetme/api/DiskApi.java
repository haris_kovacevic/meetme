package ba.hk.meetme.api;


import android.content.Context;
import android.content.SharedPreferences;

import ba.hk.meetme.App;

public class DiskApi {
    private static final String PREFS = "creative.meetme.ba";
    private final SharedPreferences mSharedPreferences;

    private static class Loader {
        static DiskApi INSTANCE = new DiskApi();
    }


    public static DiskApi getInstance() {
        return Loader.INSTANCE;
    }

    private DiskApi() {
        mSharedPreferences = App.getInstance().getSharedPreferences(PREFS, Context.MODE_MULTI_PROCESS);
    }


    //KEYS

    private static final String KEY_TOKEN = "token";
    private static final String KEY_USERNAME = "username";

    public void persistToken(String token) {
        mSharedPreferences.edit().putString(KEY_TOKEN, token).apply();
    }

    public String readToken() {
        return mSharedPreferences.getString(KEY_TOKEN, null);
    }

    public void persistUsername(String username) {
        mSharedPreferences.edit().putString(KEY_USERNAME, username).apply();
    }

    public String readUsername() {
        return mSharedPreferences.getString(KEY_USERNAME, null);
    }
}
