package ba.hk.meetme.api;


import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.TimeUnit;

import ba.hk.meetme.models.Feed;
import ba.hk.meetme.models.ImmutableFeed;
import io.reactivex.Single;

public class MockFeedsData {
    public static List<Feed> get() {
        List<Feed> people = new ArrayList<>();
        people.add(ImmutableFeed.builder().name("Ema")
                .addGallery(
                        "https://i.imgur.com/OlxrtNY.jpg",
                        "https://i.imgur.com/FJ84OW3.png",
                        "https://i.imgur.com/El5j99v.jpg",
                        "https://i.imgur.com/1YXC4cc.jpg"
                ).build());

        people.add(ImmutableFeed.builder().name("Liz")
                .addGallery(
                        "https://i.imgur.com/3n668L6.jpg",
                        "https://i.imgur.com/gnJgayo.jpg",
                        "https://i.imgur.com/E9G60sa.jpg"
                ).build());


        people.add(ImmutableFeed.builder().name("Amanda").addGallery(
                "https://i.imgur.com/6RVnMgO.jpg"
        ).build());


        people.add(ImmutableFeed.builder().name("Rita")
                .addGallery(
                        "https://i.imgur.com/pXLmSQU.jpg",
                        "https://i.imgur.com/N0gVFb8.jpg"
                ).build());

        people.add(ImmutableFeed.builder().name("Layla")
                .addGallery(
                        "https://i.imgur.com/8hOk46X.jpg",
                        "https://i.imgur.com/lO6yk27.jpg"
                ).build());

        people.add(ImmutableFeed.builder().name("Ana")
                .addGallery(
                        "https://i.imgur.com/c3S4fyQ.jpg"
                ).build());


        return people;
    }

    public static Single<ApiResult<List<Feed>>> mockApi() {
        return Single.just(get())
                .delay(2000, TimeUnit.MILLISECONDS)
                .map(mockData -> ImmutableApiResult.<List<Feed>>builder().data(get()).message("Mock data").successful(true).build());
    }
}
