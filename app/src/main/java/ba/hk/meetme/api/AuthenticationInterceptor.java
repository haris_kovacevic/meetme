package ba.hk.meetme.api;


import com.fernandocejas.arrow.optional.Optional;

import java.io.IOException;

import ba.hk.meetme.stores.TokenStore;
import okhttp3.Interceptor;
import okhttp3.Request;
import okhttp3.Response;

public class AuthenticationInterceptor implements Interceptor {

    @Override
    public Response intercept(Chain chain) throws IOException {
        Request original = chain.request();

        Optional<String> tokenOpt = TokenStore.getInstance().get();

        Request.Builder builder = original.newBuilder();

        if (tokenOpt.isPresent())
            builder.header("Authorization", String.format("Bearer %s", tokenOpt.get()));

        Request request = builder.build();

        return chain.proceed(request);
    }
}
