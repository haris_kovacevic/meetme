package ba.hk.meetme.api;


import ba.hk.meetme.models.LoginForm;
import ba.hk.meetme.models.RegistrationForm;
import ba.hk.meetme.models.Response;
import ba.hk.meetme.models.Token;
import io.reactivex.Single;
import retrofit2.adapter.rxjava2.Result;
import retrofit2.http.Body;
import retrofit2.http.GET;
import retrofit2.http.POST;
import retrofit2.http.Query;

public interface NetworkService {
    public static final String BASE_URL = "https://ci-api.favor.co/";

    @POST("account/register")
    Single<Result<Response<Token>>> register(@Body RegistrationForm form);

    @POST("account/login")
    Single<Result<Response<Token>>> login(@Body LoginForm form);

    @GET("statements")
    Single<Result<Response>> getFeed(@Query("page") int page);

}

