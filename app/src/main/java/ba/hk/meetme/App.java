package ba.hk.meetme;


import android.app.Application;

import ba.hk.meetme.manager.ChatManager;

public class App extends Application {
    private static App INSTANCE;

    public static App getInstance() {
        return INSTANCE;
    }

    @Override
    public void onCreate() {
        super.onCreate();
        INSTANCE = this;
    }
}
