package ba.hk.meetme.models;

import org.immutables.value.Value;
import org.parceler.Parcel;
import org.parceler.ParcelFactory;

import java.util.List;

@Parcel(Parcel.Serialization.VALUE)
@Value.Immutable
public abstract class Feed {
    public abstract String name();

    public abstract List<String> gallery();


    @ParcelFactory
    static Feed build(String name, List<String> gallery) {
        return ImmutableFeed.builder()
                .name(name)
                .gallery(gallery)
                .build();
    }
}
