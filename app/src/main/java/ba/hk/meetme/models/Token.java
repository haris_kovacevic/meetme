package ba.hk.meetme.models;

import org.immutables.value.Value;

@Value.Immutable
public abstract class Token {
    public abstract String token();

    public abstract String firebaseToken();
}
