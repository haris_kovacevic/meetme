package ba.hk.meetme.models;

import org.immutables.serial.Serial;
import org.immutables.value.Value;
import org.parceler.Parcel;
import org.parceler.ParcelFactory;

import java.io.Serializable;

@Parcel(Parcel.Serialization.VALUE)
@Value.Immutable
public abstract class Message {

    public abstract String sender();

    public abstract String text();

    public abstract boolean incoming();

    @ParcelFactory
    static Message build(String sender, String text) {
        return ImmutableMessage.builder()
                .sender(sender)
                .text(text)
                .build();
    }
}
