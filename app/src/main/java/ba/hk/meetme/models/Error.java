package ba.hk.meetme.models;

import android.support.annotation.Nullable;

import com.fernandocejas.arrow.optional.Optional;

import org.immutables.value.Value;

@Value.Immutable
public abstract class Error {
    @Nullable
    public abstract Integer httpCode();

    @Nullable
    public abstract String name();

    @Nullable
    public abstract String message();

    public Optional<Integer> httpCodeOpt() {
        return Optional.fromNullable(httpCode());
    }

    public Optional<String> nameOpt() {
        return Optional.fromNullable(name());
    }

    public Optional<String> messageOpt() {
        return Optional.fromNullable(message());
    }

}
