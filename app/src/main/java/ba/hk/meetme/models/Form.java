package ba.hk.meetme.models;


public interface Form {
    String email();

    String password();
}
