package ba.hk.meetme.utils;


import android.util.Patterns;

public class ValidationHelper {
    public static boolean isEmailValid(String email) {
        return email != null && Patterns.EMAIL_ADDRESS.matcher(email).matches();

    }

    public static boolean isPasswordValid(String password) {
        return password != null && password.length() >= 5;
    }
}
