package ba.hk.meetme.utils;


import android.text.TextUtils;

public class TokenUtils {
    public static boolean isValid(String token) {
        return !TextUtils.isEmpty(token);
    }
}
