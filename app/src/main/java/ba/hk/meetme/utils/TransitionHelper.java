package ba.hk.meetme.utils;


import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.view.ViewTreeObserver;

public class TransitionHelper {
    public static void scheduleStartPostponedTransition(AppCompatActivity activity, View sharedElement) {
        activity.supportPostponeEnterTransition();
        sharedElement.getViewTreeObserver().addOnPreDrawListener(
                new ViewTreeObserver.OnPreDrawListener() {
                    @Override
                    public boolean onPreDraw() {
                        sharedElement.getViewTreeObserver().removeOnPreDrawListener(this);
                        activity.supportStartPostponedEnterTransition();
                        return true;
                    }
                });
    }
}
