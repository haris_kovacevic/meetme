package ba.hk.meetme.ui.feeds;

import org.immutables.value.Value;

import java.util.List;

import ba.hk.meetme.models.Error;
import ba.hk.meetme.models.Feed;

@Value.Enclosing
public class FeedViewModelEvents {
    public static class ShowLoading extends FeedViewModelEvents {
    }

    @Value.Immutable
    public static abstract class ShowFeeds extends FeedViewModelEvents {
        public abstract List<Feed> feeds();
    }

    @Value.Immutable
    public static abstract class ShowError extends FeedViewModelEvents {
        public abstract Error error();
    }

    @Value.Immutable
    public static abstract class OpenGallery extends FeedViewModelEvents {
        public abstract Feed feed();
    }

    public static class GalleryOpened extends FeedViewModelEvents {
    }
}
