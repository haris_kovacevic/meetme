package ba.hk.meetme.ui.login;


import android.arch.lifecycle.LiveData;
import android.arch.lifecycle.MutableLiveData;
import android.arch.lifecycle.ViewModel;
import android.util.Pair;

import com.fernandocejas.arrow.optional.Optional;

import ba.hk.meetme.api.NetworkApi;
import ba.hk.meetme.manager.ChatManager;
import ba.hk.meetme.stores.TokenStore;
import ba.hk.meetme.ui.login.data.ImmutableLoginLoadResults;
import ba.hk.meetme.ui.login.data.LoginLoadResults;
import ba.hk.meetme.utils.TokenUtils;
import ba.hk.meetme.utils.ValidationHelper;
import io.reactivex.BackpressureStrategy;
import io.reactivex.Flowable;
import io.reactivex.Observable;
import io.reactivex.disposables.Disposable;
import io.reactivex.subjects.PublishSubject;

public class LoginViewModel extends ViewModel {
    private PublishSubject<LoginUiEvents> mUiEvents = PublishSubject.create();
    private MutableLiveData<LoginUiModel> mUiModel = new MutableLiveData<>();
    private Disposable mSubscription;


    public LoginViewModel() {
        mSubscription = createStream().subscribe(uiModel -> mUiModel.postValue(uiModel));
    }

    private Flowable<LoginUiModel> createStream() {

        Observable<Optional<String>> tokenStream = TokenStore.getInstance().stream();

        Observable<Pair<String, String>> input = Observable.combineLatest(
                mUiEvents.ofType(LoginUiEvents.EmailValueChanged.class).map(event -> event.email()),
                mUiEvents.ofType(LoginUiEvents.PasswordValueChanged.class).map(event -> event.password()),
                (email, password) -> Pair.create(email, password)
        );

        Observable<LoginViewModelEvents> loginUser = mUiEvents.ofType(LoginUiEvents.LoginButtonClicked.class)
                .flatMap(event -> login(event.email(), event.password())
                        .publish(loadResult ->
                                Observable.merge(
                                        loadResult.ofType(LoginLoadResults.Loaded.class).doOnNext(res -> {
                                            ChatManager.getInstance().setUser(event.email());
                                            TokenStore.getInstance().saveToken(res.data().token());
                                        }).map(loaded -> new LoginViewModelEvents.LoggedIn()),
                                        loadResult.ofType(LoginLoadResults.Failed.class).map(res -> ImmutableLoginViewModelEvents.ApiError.builder().error(res.error()).build()),
                                        loadResult.ofType(LoginLoadResults.Loading.class).map(res -> new LoginViewModelEvents.Loading())
                                )

                        ));


        Observable<LoginViewModelEvents> events = Observable.merge(
                mUiEvents.ofType(LoginUiEvents.RegisterButtonClicked.class).map(x -> new LoginViewModelEvents.StartRegistrationScreen()),
                mUiEvents.ofType(LoginUiEvents.RegisterButtonClickExecuted.class).map(x -> new LoginViewModelEvents.RegistrationScreenStarted()),
                Observable.merge(
                        loginUser,
                        input.map(pair -> ValidationHelper.isEmailValid(pair.first) && ValidationHelper.isPasswordValid(pair.second)).map(validInput -> ImmutableLoginViewModelEvents.EnableLoginButton.builder().enable(validInput).build()),
                        tokenStream.filter(tokenOpt -> tokenOpt.isPresent() && TokenUtils.isValid(tokenOpt.get())).map(x -> new LoginViewModelEvents.OpenMainScreen())
                )
        );

        LoginUiModel defaultUiModel = ImmutableLoginUiModel.builder()
                .loading(false)
                .enableLoginButton(false)
                .openRegistrationScreen(false)
                .openMainScreen(false)
                .build();

        Observable<LoginUiModel> uiModels = events.scan(defaultUiModel, (uiModel, event) -> {
            if (event instanceof LoginViewModelEvents.StartRegistrationScreen)
                return ImmutableLoginUiModel.copyOf(uiModel).withOpenRegistrationScreen(true);
            else if (event instanceof LoginViewModelEvents.RegistrationScreenStarted)
                return ImmutableLoginUiModel.copyOf(uiModel).withOpenRegistrationScreen(false);
            else if (event instanceof LoginViewModelEvents.EnableLoginButton)
                return ImmutableLoginUiModel.copyOf(uiModel).withEnableLoginButton(((LoginViewModelEvents.EnableLoginButton) event).enable());
            else if (event instanceof LoginViewModelEvents.OpenMainScreen)
                return ImmutableLoginUiModel.copyOf(uiModel).withOpenMainScreen(true);
            else if (event instanceof LoginViewModelEvents.Loading)
                return ImmutableLoginUiModel.copyOf(uiModel).withLoading(true).withError(null);
            else if (event instanceof LoginViewModelEvents.LoggedIn)
                return ImmutableLoginUiModel.copyOf(uiModel).withLoading(false).withError(null);
            else if (event instanceof LoginViewModelEvents.ApiError)
                return ImmutableLoginUiModel.copyOf(uiModel).withLoading(false).withError(((LoginViewModelEvents.ApiError) event).error());
            return uiModel;
        });

        return uiModels.toFlowable(BackpressureStrategy.LATEST);
    }

    public void postEvent(LoginUiEvents event) {
        mUiEvents.onNext(event);
    }

    public Observable<LoginLoadResults> login(String email, String password) {
        return NetworkApi.getInstance().login(email, password).toObservable().publish(responseObs ->
                Observable.merge(
                        responseObs.filter(res -> res.successful()).map(res -> ImmutableLoginLoadResults.Loaded.builder().data(res.data()).build()),
                        responseObs.filter(res -> !res.successful()).map(res -> ImmutableLoginLoadResults.Failed.builder().error(res.error()).build())
                )
        ).startWith(new LoginLoadResults.Loading()).share();
    }

    @Override
    protected void onCleared() {
        super.onCleared();
        mSubscription.dispose();
    }

    public LiveData<LoginUiModel> getUiModel() {
        return mUiModel;
    }
}
