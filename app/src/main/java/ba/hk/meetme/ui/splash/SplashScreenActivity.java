package ba.hk.meetme.ui.splash;

import android.arch.lifecycle.LiveDataReactiveStreams;
import android.arch.lifecycle.ViewModelProviders;
import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.support.annotation.Nullable;

import ba.hk.meetme.R;
import ba.hk.meetme.databinding.ActivitySplashBinding;
import ba.hk.meetme.ui.BaseActivity;
import ba.hk.meetme.ui.login.LoginActivity;
import ba.hk.meetme.ui.main.MainActivity;
import io.reactivex.Flowable;
import io.reactivex.flowables.ConnectableFlowable;

public class SplashScreenActivity extends BaseActivity {
    private ActivitySplashBinding mBinding;
    private SplashViewModel mViewModel;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mBinding = DataBindingUtil.setContentView(this, R.layout.activity_splash);
        initViewModel();
        subscribeViewModel();
    }

    private void initViewModel() {
        mViewModel = ViewModelProviders.of(this).get(SplashViewModel.class);
    }

    private void subscribeViewModel() {
        ConnectableFlowable<SplashUiModel> mUiModel = Flowable.fromPublisher(LiveDataReactiveStreams.toPublisher(this, mViewModel.getUiModel())).publish();
        mSubscriptions.addAll(
                mUiModel.map(model -> model.openLoginScreen()).distinctUntilChanged().filter(openLoginScreen -> openLoginScreen).doOnNext(x -> finish()).subscribe(x -> LoginActivity.open(this)),
                mUiModel.map(model -> model.openMainScreen()).distinctUntilChanged().filter(openMainScreen -> openMainScreen).doOnNext(x -> finish()).subscribe(x -> MainActivity.open(this))
        );

        mViewModel.postEvent(new SplashUiEvents.Created());
        mUiModel.connect();
    }
}
