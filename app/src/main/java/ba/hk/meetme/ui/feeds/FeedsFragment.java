package ba.hk.meetme.ui.feeds;


import android.arch.lifecycle.LiveDataReactiveStreams;
import android.arch.lifecycle.ViewModelProviders;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.yuyakaido.android.cardstackview.CardStackView;
import com.yuyakaido.android.cardstackview.SwipeDirection;

import java.util.List;

import ba.hk.meetme.databinding.FragmentFeedsBinding;
import ba.hk.meetme.models.Feed;
import ba.hk.meetme.ui.BaseFragment;
import ba.hk.meetme.ui.gallery.GalleryActivity;
import ba.hk.meetme.utils.Utils;
import io.reactivex.Flowable;
import io.reactivex.flowables.ConnectableFlowable;

public class FeedsFragment extends BaseFragment {
    private FragmentFeedsBinding mBinding;
    private FeedAdapter mAdapter;
    private FeedsViewModel mViewModel;

    public static FeedsFragment newInstance() {
        return new FeedsFragment();
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        mBinding = FragmentFeedsBinding.inflate(inflater, container, false);
        return mBinding.getRoot();
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        setupUI();
        initViewModel();
        subscribeViewModel();
    }

    private void setupUI() {
        mAdapter = new FeedAdapter(getContext());
        mBinding.cardContainer.setAdapter(mAdapter);
        mBinding.cardContainer.setCardEventListener(new CardEventListenerImpl());
    }

    private void initViewModel() {
        mViewModel = ViewModelProviders.of(this).get(FeedsViewModel.class);
    }

    private void subscribeViewModel() {
        ConnectableFlowable<FeedsUiModel> mUiModel = Flowable.fromPublisher(LiveDataReactiveStreams.toPublisher(this, mViewModel.getUiModel())).publish();
        mSubscriptions.addAll(
                mUiModel.map(model -> model.feedsOpt()).distinctUntilChanged().filter(opt -> opt.isPresent()).map(opt -> opt.get()).subscribe(this::showFeeds),
                mUiModel.map(model -> model.showLoading()).distinctUntilChanged().subscribe(this::showLoading),
                mUiModel.map(model -> model.openGalleryOpt()).distinctUntilChanged().filter(opt -> opt.isPresent()).map(opt -> opt.get()).subscribe(this::openGallery)
        );

        mViewModel.postEvent(new FeedUiEvents.Created());
        mUiModel.connect();
    }

    private void openGallery(Feed feed) {
        FeedAdapter.ViewHolder currentFeedViewHolder = getCurrentFeedViewHolder();
        GalleryActivity.open(
                getContext(),
                feed,
                currentFeedViewHolder != null ? currentFeedViewHolder.getImageView() : null,
                currentFeedViewHolder != null ? currentFeedViewHolder.getTextView() : null
        );
        mViewModel.postEvent(new FeedUiEvents.FeedClickExecuted());
    }

    private void showLoading(boolean loading) {
        mBinding.progress.setVisibility(loading ? View.VISIBLE : View.GONE);
    }


    private FeedAdapter.ViewHolder getCurrentFeedViewHolder() {
        ViewGroup container = mBinding.cardContainer.getTopView().getContentContainer();
        if (container != null && container.getChildCount() > 0) {
            ViewGroup card = (ViewGroup) container.getChildAt(0);
            return (FeedAdapter.ViewHolder) card.getTag();
        }
        return null;
    }


    private void showFeeds(List<Feed> feeds) {
        mAdapter.addAll(feeds);
    }

    private class CardEventListenerImpl implements CardStackView.CardEventListener {

        @Override
        public void onCardDragging(float percentX, float percentY) {
            //TO-DO route trough VM
            FeedAdapter.ViewHolder cardViewHolder = getCurrentFeedViewHolder();
            if (cardViewHolder != null)
                cardViewHolder.showSwipeStatus(percentX, percentY);
        }

        @Override
        public void onCardSwiped(SwipeDirection direction) {
            mViewModel.postEvent(
                    ImmutableFeedUiEvents
                            .FeedSwipe.builder()
                            .feed(mAdapter.getItem(mBinding.cardContainer.getTopIndex() - 1))
                            .direction(direction == SwipeDirection.Left ? FeedSwipeDirection.LEFT : FeedSwipeDirection.RIGHT)
                            .build()
            );

        }

        @Override
        public void onCardReversed() {

        }

        @Override
        public void onCardMovedToOrigin() {

        }

        @Override
        public void onCardClicked(int index) {
            mViewModel.postEvent(
                    ImmutableFeedUiEvents
                            .FeedClicked.builder()
                            .feed(mAdapter.getItem(mBinding.cardContainer.getTopIndex()))
                            .build()
            );
        }
    }

    @Override
    public void setUserVisibleHint(boolean isVisibleToUser) {
        super.setUserVisibleHint(isVisibleToUser);
        if (isVisibleToUser)
            Utils.hideSoftKeyboard(getActivity());
    }
}
