package ba.hk.meetme.ui.login;

import android.arch.lifecycle.LiveDataReactiveStreams;
import android.arch.lifecycle.ViewModelProviders;
import android.content.Context;
import android.content.Intent;
import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.View;
import android.widget.Toast;

import ba.hk.meetme.R;
import ba.hk.meetme.databinding.ActivityLoginBinding;
import ba.hk.meetme.models.Error;
import ba.hk.meetme.ui.BaseActivity;
import ba.hk.meetme.ui.main.MainActivity;
import ba.hk.meetme.ui.registration.RegistrationActivity;
import ba.hk.meetme.utils.TextChangedListener;
import io.reactivex.Flowable;
import io.reactivex.flowables.ConnectableFlowable;

public class LoginActivity extends BaseActivity {
    private ActivityLoginBinding mBinding;
    private LoginViewModel mViewModel;

    public static void open(Context context) {
        context.startActivity(new Intent(context, LoginActivity.class));
    }

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mBinding = DataBindingUtil.setContentView(this, R.layout.activity_login);
        setupUI();
        initViewModel();
        subscribeViewModel();
    }

    public void setupUI() {
        mBinding.email.getEditText().addTextChangedListener(new EmailChangedListener());
        mBinding.password.getEditText().addTextChangedListener(new PasswordChangedListener());
        mBinding.login.setOnClickListener(x -> mViewModel.postEvent(
                ImmutableLoginUiEvents.LoginButtonClicked.builder()
                        .email(mBinding.email.getEditText().getText().toString())
                        .password(mBinding.password.getEditText().getText().toString()).build())
        );
        mBinding.register.setOnClickListener(x -> mViewModel.postEvent(new LoginUiEvents.RegisterButtonClicked()));
    }

    private void initViewModel() {
        mViewModel = ViewModelProviders.of(this).get(LoginViewModel.class);
    }

    private void subscribeViewModel() {
        ConnectableFlowable<LoginUiModel> mUiModel = Flowable.fromPublisher(LiveDataReactiveStreams.toPublisher(this, mViewModel.getUiModel())).publish();
        mSubscriptions.addAll(
                mUiModel.map(model -> model.openRegistrationScreen()).distinctUntilChanged().filter(openRegistrationScreen -> openRegistrationScreen).subscribe(x -> openRegistration()),
                mUiModel.map(model -> model.enableLoginButton()).distinctUntilChanged().subscribe(this::enableLoginButton),
                mUiModel.map(model -> model.openMainScreen()).distinctUntilChanged().filter(openMainScreen -> openMainScreen).subscribe(x -> openMain()),
                mUiModel.map(model -> model.loading()).distinctUntilChanged().subscribe(this::showLoading),
                mUiModel.map(model -> model.errorOpt()).distinctUntilChanged().filter(opt -> opt.isPresent()).map(opt -> opt.get()).subscribe(this::showError)
        );

        mUiModel.connect();
    }

    private void showLoading(boolean loading) {
        mBinding.loader.getRoot().setVisibility(loading ? View.VISIBLE : View.GONE);
    }

    private void openMain() {
        MainActivity.open(this);
        finish();
    }

    private void enableLoginButton(boolean enable) {
        mBinding.login.setAlpha(enable ? 1 : .8f);
        mBinding.login.setEnabled(enable);
    }

    private void showError(Error error) {
        Toast.makeText(this, error.message(), Toast.LENGTH_SHORT).show();
    }

    private void openRegistration() {
        RegistrationActivity.open(this);
        mViewModel.postEvent(new LoginUiEvents.RegisterButtonClickExecuted());
    }

    private class EmailChangedListener extends TextChangedListener {

        @Override
        public void onTextChanged(String text) {
            mViewModel.postEvent(ImmutableLoginUiEvents.EmailValueChanged.builder().email(text).build());
        }
    }

    private class PasswordChangedListener extends TextChangedListener {

        @Override
        public void onTextChanged(String text) {
            mViewModel.postEvent(ImmutableLoginUiEvents.PasswordValueChanged.builder().password(text).build());
        }
    }

}
