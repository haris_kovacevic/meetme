package ba.hk.meetme.ui.feeds.data;

import org.immutables.value.Value;

import java.util.List;

import ba.hk.meetme.models.Error;
import ba.hk.meetme.models.Feed;

@Value.Enclosing
public class FeedsLoadResults {
    public static class Loading extends FeedsLoadResults {
    }

    @Value.Immutable
    public static abstract class Loaded extends FeedsLoadResults {
        public abstract List<Feed> feeds();
    }

    @Value.Immutable
    public static abstract class Failed extends FeedsLoadResults {
        public abstract Error error();
    }
}
