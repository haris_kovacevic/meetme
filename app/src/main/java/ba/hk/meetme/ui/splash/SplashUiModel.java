package ba.hk.meetme.ui.splash;

import org.immutables.value.Value;

@Value.Immutable
public abstract class SplashUiModel {
    public abstract boolean openLoginScreen();

    public abstract boolean openMainScreen();
}
