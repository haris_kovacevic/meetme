package ba.hk.meetme.ui.splash;

import org.immutables.value.Value;

@Value.Enclosing
public class SplashUiEvents {
    public static class Created extends SplashUiEvents {
    }
}
