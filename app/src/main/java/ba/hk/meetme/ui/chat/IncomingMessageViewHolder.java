package ba.hk.meetme.ui.chat;


import android.support.v7.widget.RecyclerView;

import ba.hk.meetme.databinding.ItemIncomingMessageBinding;
import ba.hk.meetme.models.Message;

public class IncomingMessageViewHolder extends RecyclerView.ViewHolder {
    private final ItemIncomingMessageBinding mBinding;

    public IncomingMessageViewHolder(ItemIncomingMessageBinding binding) {
        super(binding.getRoot());
        mBinding = binding;
    }

    public void bind(Message message) {
        mBinding.message.setText(message.text());
    }
}
