package ba.hk.meetme.ui.registration;

import org.immutables.value.Value;

import ba.hk.meetme.models.Error;

@Value.Enclosing
public class RegistrationViewModelEvents {

    public static class Loading extends RegistrationViewModelEvents {
    }

    public static class Registered extends RegistrationViewModelEvents {
    }

    @Value.Immutable
    public static abstract class ApiError extends RegistrationViewModelEvents {
        public abstract Error error();
    }

    public static class OpenMainScreen extends RegistrationViewModelEvents {
    }

    @Value.Immutable
    public static abstract class EnableRegisterButton extends RegistrationViewModelEvents {
        public abstract boolean enable();
    }
}
