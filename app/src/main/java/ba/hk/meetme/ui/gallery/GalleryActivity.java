package ba.hk.meetme.ui.gallery;


import android.app.Activity;
import android.arch.lifecycle.LiveDataReactiveStreams;
import android.arch.lifecycle.ViewModelProviders;
import android.content.Context;
import android.content.Intent;
import android.databinding.DataBindingUtil;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.ActivityOptionsCompat;
import android.support.v4.util.Pair;
import android.support.v4.view.ViewCompat;
import android.transition.Fade;
import android.view.View;


import org.parceler.Parcels;

import ba.hk.meetme.R;
import ba.hk.meetme.databinding.ActivityGalleryBinding;
import ba.hk.meetme.models.Feed;
import ba.hk.meetme.ui.BaseActivity;
import ba.hk.meetme.utils.TransitionHelper;
import io.reactivex.Flowable;
import io.reactivex.flowables.ConnectableFlowable;

public class GalleryActivity extends BaseActivity {
    private static final String EXTRA_FEED = "feed";
    private ActivityGalleryBinding mBinding;
    private GalleryViewModel mViewModel;

    public static void open(Context context, Feed feed, View image,View name) {
        Intent intent = new Intent(context, GalleryActivity.class);
        intent.putExtra(EXTRA_FEED, Parcels.wrap(Feed.class, feed));

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP && image != null) {
            Pair<View, String> p1 = Pair.create(image, ViewCompat.getTransitionName(image));
            Pair<View, String> p2 = Pair.create(name, ViewCompat.getTransitionName(name));

            ActivityOptionsCompat options = ActivityOptionsCompat.makeSceneTransitionAnimation((Activity) context, p1,p2);
            context.startActivity(intent, options.toBundle());
        } else {
            context.startActivity(intent);
        }
    }

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mBinding = DataBindingUtil.setContentView(this, R.layout.activity_gallery);
        scheduleTransition();
        initViewModel();
        subscribeViewModel();
    }

    private void scheduleTransition() {
        TransitionHelper.scheduleStartPostponedTransition(this, mBinding.gallery);
    }

    private void initViewModel() {
        mViewModel = ViewModelProviders.of(this).get(GalleryViewModel.class);
    }

    private void subscribeViewModel() {
        ConnectableFlowable<GalleryUiModel> mUiModel = Flowable.fromPublisher(LiveDataReactiveStreams.toPublisher(this, mViewModel.getUiModel())).publish();
        mSubscriptions.add(
                mUiModel.map(model -> model.showGalleryForFeedOpt()).distinctUntilChanged().filter(opt -> opt.isPresent()).map(opt -> opt.get()).subscribe(this::showGallery)
        );

        mViewModel.postEvent(ImmutableGalleryUiEvents.Created.builder().feed(Parcels.unwrap(getIntent().getParcelableExtra(EXTRA_FEED))).build());
        mUiModel.connect();
    }

    private void showGallery(Feed feed) {
        mBinding.gallery.setAdapter(new GalleryAdapter(feed.gallery(), this));
        mBinding.name.setText(feed.name());
    }

}
