package ba.hk.meetme.ui.chat;

import org.immutables.value.Value;

import ba.hk.meetme.models.Message;

@Value.Enclosing
public class ChatViewModelEvents {
    public static class ShowLoading {
    }

    @Value.Immutable
    public static abstract class ShowMessage extends ChatViewModelEvents {
        public abstract Message message();
    }
}
