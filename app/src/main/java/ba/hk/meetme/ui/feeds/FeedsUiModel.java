package ba.hk.meetme.ui.feeds;

import android.support.annotation.Nullable;

import com.fernandocejas.arrow.optional.Optional;

import org.immutables.value.Value;

import java.util.List;

import ba.hk.meetme.models.Feed;

@Value.Immutable
public abstract class FeedsUiModel {
    public abstract boolean showLoading();

    @Nullable
    public abstract List<Feed> feeds();

    @Nullable
    public abstract SwipedFeed swipedFeed();

    @Nullable
    public abstract Feed openGallery();

    public Optional<List<Feed>> feedsOpt() {
        return Optional.fromNullable(feeds());
    }

    public Optional<Feed> openGalleryOpt() {
        return Optional.fromNullable(openGallery());
    }

    public Optional<SwipedFeed> swipedFeedOpt() {
        return Optional.fromNullable(swipedFeed());
    }
}
