package ba.hk.meetme.ui.gallery;


import org.immutables.value.Value;

import ba.hk.meetme.models.Feed;

@Value.Enclosing
public class GalleryUiEvents {

    @Value.Immutable
    public static abstract class Created extends GalleryUiEvents {
        public abstract Feed feed();
    }
}
