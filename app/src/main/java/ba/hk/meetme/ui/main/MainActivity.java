package ba.hk.meetme.ui.main;


import android.content.Context;
import android.content.Intent;
import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.support.annotation.Nullable;

import ba.hk.meetme.R;
import ba.hk.meetme.databinding.ActivityMainBinding;
import ba.hk.meetme.ui.BaseActivity;

public class MainActivity extends BaseActivity {
    private ActivityMainBinding mBinding;

    public static void open(Context context) {
        context.startActivity(new Intent(context, MainActivity.class));
    }

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mBinding = DataBindingUtil.setContentView(this, R.layout.activity_main);
        setupUI();
    }

    private void setupUI() {
        mBinding.tabs.setupWithViewPager(mBinding.pager);
        mBinding.pager.setAdapter(MainFragmentPagerAdapter.create(this, getSupportFragmentManager()));
    }
}
