package ba.hk.meetme.ui.login.data;

import org.immutables.value.Value;

import ba.hk.meetme.models.Error;
import ba.hk.meetme.models.Token;

@Value.Enclosing
public class LoginLoadResults {
    public static class Loading extends LoginLoadResults {
    }

    @Value.Immutable
    public static abstract class Loaded extends LoginLoadResults {
        public abstract Token data();
    }

    @Value.Immutable
    public static abstract class Failed extends LoginLoadResults {
        public abstract Error error();
    }
}
