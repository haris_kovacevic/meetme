package ba.hk.meetme.ui.login;

import org.immutables.value.Value;

import ba.hk.meetme.models.Error;

@Value.Enclosing
public class LoginViewModelEvents {
    public static class Loading extends LoginViewModelEvents {
    }

    public static class LoggedIn extends LoginViewModelEvents {
    }

    @Value.Immutable
    public static abstract class ApiError extends LoginViewModelEvents {
        public abstract Error error();
    }

    public static class StartRegistrationScreen extends LoginViewModelEvents {
    }

    public static class RegistrationScreenStarted extends LoginViewModelEvents {
    }

    public static class OpenMainScreen extends LoginViewModelEvents {

    }

    @Value.Immutable
    public static abstract class EnableLoginButton extends LoginViewModelEvents {
        public abstract boolean enable();
    }
}
