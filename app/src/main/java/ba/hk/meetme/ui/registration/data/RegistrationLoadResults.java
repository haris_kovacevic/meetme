package ba.hk.meetme.ui.registration.data;

import org.immutables.value.Value;

import ba.hk.meetme.models.Error;
import ba.hk.meetme.models.Token;

@Value.Enclosing
public class RegistrationLoadResults {
    public static class Loading extends RegistrationLoadResults {
    }

    @Value.Immutable
    public static abstract class Loaded extends RegistrationLoadResults {
        public abstract Token data();
    }

    @Value.Immutable
    public static abstract class Failed extends RegistrationLoadResults {
        public abstract Error error();
    }
}
