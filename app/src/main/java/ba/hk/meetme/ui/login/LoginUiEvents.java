package ba.hk.meetme.ui.login;

import org.immutables.value.Value;

@Value.Enclosing
public abstract class LoginUiEvents {

    @Value.Immutable
    public static abstract class LoginButtonClicked extends LoginUiEvents {
        public abstract String email();

        public abstract String password();
    }

    public static class RegisterButtonClicked extends LoginUiEvents {

    }

    public static class RegisterButtonClickExecuted extends LoginUiEvents {

    }

    @Value.Immutable
    public static abstract class EmailValueChanged extends LoginUiEvents {
        public abstract String email();
    }

    @Value.Immutable
    public static abstract class PasswordValueChanged extends LoginUiEvents {
        public abstract String password();
    }
}
