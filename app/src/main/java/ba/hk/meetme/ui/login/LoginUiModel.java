package ba.hk.meetme.ui.login;

import android.support.annotation.Nullable;

import com.fernandocejas.arrow.optional.Optional;

import org.immutables.value.Value;

import ba.hk.meetme.models.Error;

@Value.Immutable
public abstract class LoginUiModel {
    public abstract boolean loading();

    public abstract boolean enableLoginButton();

    public abstract boolean openRegistrationScreen();

    public abstract boolean openMainScreen();


    @Nullable
    public abstract Error error();

    public Optional<Error> errorOpt() {
        return Optional.fromNullable(error());
    }

}
