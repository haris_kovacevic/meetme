package ba.hk.meetme.ui.registration;


import android.arch.lifecycle.LiveDataReactiveStreams;
import android.arch.lifecycle.ViewModelProviders;
import android.content.Context;
import android.content.Intent;
import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.View;
import android.widget.Toast;

import ba.hk.meetme.R;
import ba.hk.meetme.databinding.ActivityRegisterBinding;
import ba.hk.meetme.models.Error;
import ba.hk.meetme.ui.BaseActivity;
import ba.hk.meetme.ui.main.MainActivity;
import ba.hk.meetme.utils.TextChangedListener;
import io.reactivex.Flowable;
import io.reactivex.flowables.ConnectableFlowable;

public class RegistrationActivity extends BaseActivity {
    private ActivityRegisterBinding mBinding;
    private RegistrationViewModel mViewModel;

    public static void open(Context context) {
        context.startActivity(new Intent(context, RegistrationActivity.class));
    }

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mBinding = DataBindingUtil.setContentView(this, R.layout.activity_register);
        initViewModel();
        subscribeViewModel();
        setupUI();
    }

    private void setupUI() {
        mBinding.email.getEditText().addTextChangedListener(new EmailChangedListener());
        mBinding.password.getEditText().addTextChangedListener(new PasswordChangedListener());
        mBinding.register.setOnClickListener(
                x -> mViewModel.postEvent(ImmutableRegistrationUiEvents.RegisterButtonClicked.builder()
                        .email(mBinding.email.getEditText().getText().toString())
                        .password(mBinding.password.getEditText().getText().toString()).build())
        );
    }

    private void initViewModel() {
        mViewModel = ViewModelProviders.of(this).get(RegistrationViewModel.class);
    }

    private void subscribeViewModel() {
        ConnectableFlowable<RegistrationUiModel> mUiModel = Flowable.fromPublisher(LiveDataReactiveStreams.toPublisher(this, mViewModel.getUiModel())).publish();
        mSubscriptions.addAll(
                mUiModel.map(model -> model.enableRegistrationButton()).distinctUntilChanged().subscribe(this::enableRegistrationButton),
                mUiModel.map(model -> model.openMainScreen()).distinctUntilChanged().filter(openMainScreen -> openMainScreen).subscribe(x -> openMain()),
                mUiModel.map(model -> model.loading()).distinctUntilChanged().subscribe(this::showLoading),
                mUiModel.map(model -> model.errorOpt()).distinctUntilChanged().filter(opt -> opt.isPresent()).map(opt -> opt.get()).subscribe(this::showError)
        );

        mUiModel.connect();
    }

    private void showError(Error error) {
        Toast.makeText(this, error.message(), Toast.LENGTH_SHORT).show();
    }

    private void enableRegistrationButton(boolean enable) {
        mBinding.register.setAlpha(enable ? 1 : .8f);
        mBinding.register.setEnabled(enable);
    }

    private void showLoading(boolean loading) {
        mBinding.loader.getRoot().setVisibility(loading ? View.VISIBLE : View.GONE);
    }

    private void openMain() {
        MainActivity.open(this);
        finish();
    }


    private class EmailChangedListener extends TextChangedListener {

        @Override
        public void onTextChanged(String text) {
            mViewModel.postEvent(ImmutableRegistrationUiEvents.EmailValueChanged.builder().email(text).build());
        }
    }

    private class PasswordChangedListener extends TextChangedListener {

        @Override
        public void onTextChanged(String text) {
            mViewModel.postEvent(ImmutableRegistrationUiEvents.PasswordValueChanged.builder().password(text).build());
        }
    }
}
