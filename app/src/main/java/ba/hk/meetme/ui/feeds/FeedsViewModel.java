package ba.hk.meetme.ui.feeds;


import android.arch.lifecycle.LiveData;
import android.arch.lifecycle.MutableLiveData;
import android.arch.lifecycle.ViewModel;

import java.util.List;

import ba.hk.meetme.api.NetworkApi;
import ba.hk.meetme.models.Feed;
import ba.hk.meetme.ui.feeds.data.FeedsLoadResults;
import ba.hk.meetme.ui.feeds.data.ImmutableFeedsLoadResults;
import io.reactivex.BackpressureStrategy;
import io.reactivex.Flowable;
import io.reactivex.Observable;
import io.reactivex.disposables.Disposable;
import io.reactivex.subjects.PublishSubject;

public class FeedsViewModel extends ViewModel {
    private PublishSubject<FeedUiEvents> mUiEvents = PublishSubject.create();
    private MutableLiveData<FeedsUiModel> mUiModel = new MutableLiveData<>();
    private Disposable mSubscription;


    public FeedsViewModel() {
        mSubscription = createStream().subscribe(uiModel -> mUiModel.postValue(uiModel));
    }

    private Flowable<FeedsUiModel> createStream() {

        Observable<FeedViewModelEvents> initialLoad = mUiEvents.ofType(FeedUiEvents.Created.class).firstElement().toObservable().flatMap(x -> loadFeeds(1))
                .publish(loadResults -> Observable.merge(
                        loadResults.ofType(FeedsLoadResults.Loaded.class).map(res -> ImmutableFeedViewModelEvents.ShowFeeds.builder().feeds(res.feeds()).build()),
                        loadResults.ofType(FeedsLoadResults.Failed.class).map(res -> ImmutableFeedViewModelEvents.ShowError.builder().error(res.error()).build()),
                        loadResults.ofType(FeedsLoadResults.Loading.class).map(res -> new FeedViewModelEvents.ShowLoading())
                ));


        Observable<FeedViewModelEvents> events = Observable.merge(
                initialLoad,
                mUiEvents.ofType(FeedUiEvents.FeedClicked.class).map(ev -> ImmutableFeedViewModelEvents.OpenGallery.builder().feed(ev.feed()).build()),
                mUiEvents.ofType(FeedUiEvents.FeedClickExecuted.class).map(ev -> new FeedViewModelEvents.GalleryOpened())
        );

        FeedsUiModel defaultUiModel = ImmutableFeedsUiModel
                .builder()
                .showLoading(false)
                .build();

        Observable<FeedsUiModel> uiModels = events.scan(defaultUiModel, (oldModel, event) -> {
            if (event instanceof FeedViewModelEvents.ShowLoading)
                return ImmutableFeedsUiModel.copyOf(oldModel).withShowLoading(true).withFeeds((List<Feed>) null);
            else if (event instanceof FeedViewModelEvents.ShowFeeds)
                return ImmutableFeedsUiModel.copyOf(oldModel).withShowLoading(false).withFeeds(((FeedViewModelEvents.ShowFeeds) event).feeds());
            else if (event instanceof FeedViewModelEvents.OpenGallery)
                return ImmutableFeedsUiModel.copyOf(oldModel).withOpenGallery(((FeedViewModelEvents.OpenGallery) event).feed());
            else if (event instanceof FeedViewModelEvents.GalleryOpened)
                return ImmutableFeedsUiModel.copyOf(oldModel).withOpenGallery(null);
            return oldModel;
        });

        return uiModels.toFlowable(BackpressureStrategy.LATEST);
    }

    public void postEvent(FeedUiEvents event) {
        mUiEvents.onNext(event);
    }

    public Observable<FeedsLoadResults> loadFeeds(int page) {
        return NetworkApi.getInstance().getFeeds(page).toObservable().publish(responseObs ->
                Observable.merge(
                        responseObs.filter(res -> res.successful()).map(res -> ImmutableFeedsLoadResults.Loaded.builder().feeds(res.data()).build()),
                        responseObs.filter(res -> !res.successful()).map(res -> ImmutableFeedsLoadResults.Failed.builder().error(res.error()).build())
                )
        ).startWith(new FeedsLoadResults.Loading()).share();
    }

    @Override
    protected void onCleared() {
        super.onCleared();
        mSubscription.dispose();
    }

    public LiveData<FeedsUiModel> getUiModel() {
        return mUiModel;
    }
}
