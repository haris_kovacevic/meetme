package ba.hk.meetme.ui.chat;


import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.ViewGroup;

import java.util.List;

import ba.hk.meetme.databinding.ItemIncomingMessageBinding;
import ba.hk.meetme.databinding.ItemOutgoingMessageBinding;
import ba.hk.meetme.models.Message;

public class ChatAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {
    private static final int VIEW_TYPE_OUTGOING = 1;
    private static final int VIEW_TYPE_INCOMING = 2;

    private List<Message> mMessages;


    public ChatAdapter(List<Message> messages) {
        this.mMessages = messages;
    }

    @Override
    public int getItemViewType(int position) {
        if (mMessages.get(position).incoming())
            return VIEW_TYPE_INCOMING;
        return VIEW_TYPE_OUTGOING;
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        LayoutInflater layoutInflater = LayoutInflater.from(parent.getContext());
        if (viewType == VIEW_TYPE_OUTGOING)
            return new OutgoingMessageViewHolder(ItemOutgoingMessageBinding.inflate(layoutInflater, parent, false));
        return new IncomingMessageViewHolder(ItemIncomingMessageBinding.inflate(layoutInflater, parent, false));

    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {
        if (holder instanceof IncomingMessageViewHolder)
            ((IncomingMessageViewHolder) holder).bind(mMessages.get(position));
        else if (holder instanceof OutgoingMessageViewHolder)
            ((OutgoingMessageViewHolder) holder).bind(mMessages.get(position));
    }

    @Override
    public int getItemCount() {
        return mMessages.size();
    }

    public void addMessage(Message message) {
        mMessages.add(message);
        notifyDataSetChanged();
    }
}
