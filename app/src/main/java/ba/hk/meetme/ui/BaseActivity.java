package ba.hk.meetme.ui;

import android.support.v7.app.AppCompatActivity;

import io.reactivex.disposables.CompositeDisposable;

public class BaseActivity extends AppCompatActivity {
    protected CompositeDisposable mSubscriptions = new CompositeDisposable();


    @Override
    protected void onDestroy() {
        super.onDestroy();
        mSubscriptions.dispose();
    }
}
