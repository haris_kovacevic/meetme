package ba.hk.meetme.ui.registration;

import android.support.annotation.Nullable;

import com.fernandocejas.arrow.optional.Optional;

import org.immutables.value.Value;

import ba.hk.meetme.models.Error;

@Value.Immutable
public abstract class RegistrationUiModel {
    public abstract boolean loading();

    public abstract boolean enableRegistrationButton();

    public abstract boolean openMainScreen();

    @Nullable
    public abstract Error error();

    public Optional<Error> errorOpt() {
        return Optional.fromNullable(error());
    }
}
