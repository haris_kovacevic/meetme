package ba.hk.meetme.ui.feeds;

import org.immutables.value.Value;

import ba.hk.meetme.models.Feed;

@Value.Enclosing
public class FeedUiEvents {
    public static class Created extends FeedUiEvents {
    }

    @Value.Immutable
    public static abstract class FeedClicked extends FeedUiEvents {
        public abstract Feed feed();
    }

    public static class FeedClickExecuted extends FeedUiEvents {
    }

    @Value.Immutable
    public static abstract class FeedSwipe extends FeedUiEvents {
        public abstract FeedSwipeDirection direction();

        public abstract Feed feed();
    }

    public static abstract class FeedSwipeExecuted extends FeedUiEvents {
    }

}
