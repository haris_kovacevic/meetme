package ba.hk.meetme.ui.main;


import android.content.Context;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;

import java.util.ArrayList;

import ba.hk.meetme.R;
import ba.hk.meetme.ui.chat.ChatFragment;
import ba.hk.meetme.ui.feeds.FeedsFragment;

public class MainFragmentPagerAdapter extends FragmentPagerAdapter {
    private ArrayList<Fragment> mFragments;
    private Context mContext;

    public MainFragmentPagerAdapter(Context context, FragmentManager fm) {
        super(fm);
        mContext = context;
        mFragments = new ArrayList<>();
        mFragments.add(FeedsFragment.newInstance());
        mFragments.add(ChatFragment.newInstance());
    }


    @Override
    public Fragment getItem(int position) {
        return mFragments.get(position);
    }

    @Override
    public int getCount() {
        return mFragments.size();
    }

    @Nullable
    @Override
    public CharSequence getPageTitle(int position) {
        switch (position) {
            case 0:
                return mContext.getString(R.string.main_title_feeds);
            case 1:
                return mContext.getString(R.string.main_title_chat);
            default:
                return "";
        }
    }

    public static MainFragmentPagerAdapter create(Context context, FragmentManager fm) {
        return new MainFragmentPagerAdapter(context, fm);
    }
}
