package ba.hk.meetme.ui.gallery;


import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v4.view.PagerAdapter;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestOptions;

import java.util.List;

import ba.hk.meetme.databinding.ItemGalleryBinding;

public class GalleryAdapter extends PagerAdapter {
    private static final int FIRST_POSITION = 0;

    private List<String> mUrls;
    private Context mContext;

    public GalleryAdapter(List<String> mUrls, Context mContext) {
        this.mUrls = mUrls;
        this.mContext = mContext;
    }

    @Override
    public int getCount() {
        return mUrls.size();
    }

    @Override
    public boolean isViewFromObject(@NonNull View view, @NonNull Object object) {
        return view == object;
    }

    @Override
    public Object instantiateItem(ViewGroup collection, int position) {
        LayoutInflater inflater = LayoutInflater.from(mContext);
        ItemGalleryBinding binding = ItemGalleryBinding.inflate(inflater, collection, false);
        Glide.with(binding.image.getContext())
                .load(mUrls.get(position))
                .apply(new RequestOptions().centerCrop())
                .into(binding.image);
        collection.addView(binding.getRoot());

        if (position == FIRST_POSITION)
            binding.image.setTransitionName("image");

        return binding.getRoot();
    }

    @Override
    public void destroyItem(ViewGroup collection, int position, Object view) {
        collection.removeView((View) view);
    }


}
