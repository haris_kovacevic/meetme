package ba.hk.meetme.ui.chat;

import android.support.annotation.Nullable;

import com.fernandocejas.arrow.optional.Optional;

import org.immutables.value.Value;

import ba.hk.meetme.models.Message;

@Value.Immutable
public abstract class ChatUiModel {
    @Nullable
    public abstract Message showMessage();

    public Optional<Message> showMessageOpt() {
        return Optional.fromNullable(showMessage());
    }
}
