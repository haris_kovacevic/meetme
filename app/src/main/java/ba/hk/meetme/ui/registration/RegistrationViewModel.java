package ba.hk.meetme.ui.registration;


import android.arch.lifecycle.LiveData;
import android.arch.lifecycle.MutableLiveData;
import android.arch.lifecycle.ViewModel;

import com.fernandocejas.arrow.optional.Optional;

import ba.hk.meetme.api.NetworkApi;
import ba.hk.meetme.manager.ChatManager;
import ba.hk.meetme.stores.TokenStore;
import ba.hk.meetme.ui.registration.data.ImmutableRegistrationLoadResults;
import ba.hk.meetme.ui.registration.data.RegistrationLoadResults;
import ba.hk.meetme.utils.TokenUtils;
import ba.hk.meetme.utils.ValidationHelper;
import io.reactivex.BackpressureStrategy;
import io.reactivex.Flowable;
import io.reactivex.Observable;
import io.reactivex.disposables.Disposable;
import io.reactivex.subjects.PublishSubject;

public class RegistrationViewModel extends ViewModel {
    private PublishSubject<RegistrationUiEvents> mUiEvents = PublishSubject.create();
    private MutableLiveData<RegistrationUiModel> mUiModel = new MutableLiveData<>();
    private Disposable mSubscription;


    public RegistrationViewModel() {
        mSubscription = createStream().subscribe(uiModel -> mUiModel.postValue(uiModel));
    }

    private Flowable<RegistrationUiModel> createStream() {
        Observable<Optional<String>> tokenStream = TokenStore.getInstance().stream();


        Observable<RegistrationViewModelEvents> registerUser = mUiEvents.ofType(RegistrationUiEvents.RegisterButtonClicked.class).flatMap(ev -> register(ev.email(), ev.password()).publish(loadResult ->
                Observable.merge(
                        loadResult.ofType(RegistrationLoadResults.Loaded.class).doOnNext(res -> {
                            ChatManager.getInstance().setUser(ev.email());
                            TokenStore.getInstance().saveToken(res.data().token());
                        }).map(loaded -> new RegistrationViewModelEvents.Registered()),
                        loadResult.ofType(RegistrationLoadResults.Failed.class).map(res -> ImmutableRegistrationViewModelEvents.ApiError.builder().error(res.error()).build()),
                        loadResult.ofType(RegistrationLoadResults.Loading.class).map(res -> new RegistrationViewModelEvents.Loading())
                )

        ));

        Observable<RegistrationViewModelEvents> events = Observable.merge(
                Observable.combineLatest(
                        mUiEvents.ofType(RegistrationUiEvents.EmailValueChanged.class).map(event -> event.email()),
                        mUiEvents.ofType(RegistrationUiEvents.PasswordValueChanged.class).map(event -> event.password()),
                        (email, password) -> ValidationHelper.isEmailValid(email) && ValidationHelper.isPasswordValid(password)
                ).
                        map(validInput -> ImmutableRegistrationViewModelEvents.EnableRegisterButton.builder().enable(validInput).build()),
                registerUser,
                tokenStream.filter(tokenOpt -> tokenOpt.isPresent() && TokenUtils.isValid(tokenOpt.get())).map(x -> new RegistrationViewModelEvents.OpenMainScreen())
        );

        RegistrationUiModel defaultUiModel = ImmutableRegistrationUiModel.builder()
                .loading(false)
                .enableRegistrationButton(false)
                .openMainScreen(false)
                .build();

        Observable<RegistrationUiModel> uiModels = events.scan(defaultUiModel, (oldModel, event) -> {
            if (event instanceof RegistrationViewModelEvents.Loading)
                return ImmutableRegistrationUiModel.copyOf(oldModel).withLoading(true).withError(null);
            else if (event instanceof RegistrationViewModelEvents.ApiError)
                return ImmutableRegistrationUiModel.copyOf(oldModel).withLoading(false).withError(((RegistrationViewModelEvents.ApiError) event).error());
            else if (event instanceof RegistrationViewModelEvents.OpenMainScreen)
                return ImmutableRegistrationUiModel.copyOf(oldModel).withOpenMainScreen(true);
            else if (event instanceof RegistrationViewModelEvents.EnableRegisterButton)
                return ImmutableRegistrationUiModel.copyOf(oldModel).withEnableRegistrationButton(((RegistrationViewModelEvents.EnableRegisterButton) event).enable());
            return oldModel;
        });

        return uiModels.toFlowable(BackpressureStrategy.LATEST);
    }

    private Observable<RegistrationLoadResults> register(String email, String password) {
        return NetworkApi.getInstance().register(email, password).toObservable().publish(responseObs ->
                Observable.merge(
                        responseObs.filter(res -> res.successful()).map(res -> ImmutableRegistrationLoadResults.Loaded.builder().data(res.data()).build()),
                        responseObs.filter(res -> !res.successful()).map(res -> ImmutableRegistrationLoadResults.Failed.builder().error(res.error()).build())
                )
        ).startWith(new RegistrationLoadResults.Loading()).share();
    }

    public void postEvent(RegistrationUiEvents event) {
        mUiEvents.onNext(event);
    }


    @Override
    protected void onCleared() {
        super.onCleared();
        mSubscription.dispose();
    }

    public LiveData<RegistrationUiModel> getUiModel() {
        return mUiModel;
    }
}
