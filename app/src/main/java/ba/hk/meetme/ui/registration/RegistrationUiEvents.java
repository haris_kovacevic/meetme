package ba.hk.meetme.ui.registration;

import org.immutables.value.Value;

@Value.Enclosing
public class RegistrationUiEvents {

    @Value.Immutable
    public static abstract class RegisterButtonClicked extends RegistrationUiEvents {
        public abstract String email();

        public abstract String password();
    }


    @Value.Immutable
    public static abstract class EmailValueChanged extends RegistrationUiEvents {
        public abstract String email();
    }

    @Value.Immutable
    public static abstract class PasswordValueChanged extends RegistrationUiEvents {
        public abstract String password();
    }
}
