package ba.hk.meetme.ui.feeds;


import android.content.Context;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestOptions;

import ba.hk.meetme.databinding.ItemCardBinding;
import ba.hk.meetme.models.Feed;

public class FeedAdapter extends ArrayAdapter<Feed> {
    private LayoutInflater mInflater;

    public FeedAdapter(@NonNull Context context) {
        super(context, 0);
        mInflater = LayoutInflater.from(getContext());
    }

    @NonNull
    @Override
    public View getView(int position, @Nullable View convertView, @NonNull ViewGroup parent) {
        ViewHolder holder;

        if (convertView == null) {
            ItemCardBinding binding = ItemCardBinding.inflate(mInflater, parent, false);
            convertView = binding.getRoot();
            holder = new ViewHolder(binding);
            convertView.setTag(holder);
        } else {
            holder = (ViewHolder) convertView.getTag();
        }

        holder.bind(getItem(position));
        return convertView;
    }

    public static class ViewHolder {
        private final ItemCardBinding mBinding;

        public ViewHolder(ItemCardBinding binding) {
            mBinding = binding;
        }

        public void bind(Feed person) {
            resetSwipeStatus();
            mBinding.name.setText(person.name());
            Glide.with(mBinding.image.getContext())
                    .load(person.gallery().get(0))
                    .apply(new RequestOptions().centerCrop())
                    .into(mBinding.image);
        }

        public void showSwipeStatus(float percentX, float percentY) {
            float alpha = (Math.abs(percentX) * 2);
            mBinding.yes.setAlpha(percentX > 0 ? alpha : 0);
            mBinding.no.setAlpha(percentX < 0 ? alpha : 0);
        }

        public void resetSwipeStatus() {
            mBinding.yes.setAlpha(0);
            mBinding.no.setAlpha(0);
        }

        public ImageView getImageView() {
            return mBinding.image;
        }

        public TextView getTextView() {
            return mBinding.name;
        }
    }


}