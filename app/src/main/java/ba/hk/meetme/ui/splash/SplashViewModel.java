package ba.hk.meetme.ui.splash;


import android.arch.lifecycle.LiveData;
import android.arch.lifecycle.MutableLiveData;
import android.arch.lifecycle.ViewModel;

import ba.hk.meetme.stores.TokenStore;
import io.reactivex.BackpressureStrategy;
import io.reactivex.Flowable;
import io.reactivex.Observable;
import io.reactivex.disposables.Disposable;
import io.reactivex.subjects.PublishSubject;

public class SplashViewModel extends ViewModel {
    private PublishSubject<SplashUiEvents> mUiEvents = PublishSubject.create();
    private MutableLiveData<SplashUiModel> mUiModel = new MutableLiveData<>();
    private Disposable mSubscription;


    public SplashViewModel() {
        mSubscription = createStream().subscribe(uiModel -> mUiModel.postValue(uiModel));
    }

    private Flowable<SplashUiModel> createStream() {
        Observable<SplashViewModelEvents> viewModelEvents = mUiEvents.ofType(SplashUiEvents.Created.class).flatMap(e -> TokenStore.getInstance().stream().firstElement().toObservable()
                .publish(tokenObs ->
                        Observable.merge(
                                tokenObs.filter(opt -> !opt.isPresent()).map(x -> new SplashViewModelEvents.OpenLoginScreen()),
                                tokenObs.filter(opt -> opt.isPresent()).map(x -> new SplashViewModelEvents.OpenMainScreen())
                        )
                ));

        SplashUiModel defaultUiModel = ImmutableSplashUiModel.builder()
                .openLoginScreen(false)
                .openMainScreen(false)
                .build();


        Observable<SplashUiModel> uiModels = viewModelEvents.scan(defaultUiModel, (uiModel, events) -> {
            if (events instanceof SplashViewModelEvents.OpenLoginScreen)
                return ImmutableSplashUiModel.builder().openLoginScreen(true).openMainScreen(false).build();
            if (events instanceof SplashViewModelEvents.OpenMainScreen)
                return ImmutableSplashUiModel.builder().openLoginScreen(false).openMainScreen(true).build();
            return uiModel;
        });

        return uiModels.toFlowable(BackpressureStrategy.LATEST);
    }


    public LiveData<SplashUiModel> getUiModel() {
        return mUiModel;
    }

    public void postEvent(SplashUiEvents event) {
        mUiEvents.onNext(event);
    }

    @Override
    protected void onCleared() {
        super.onCleared();
        mSubscription.dispose();
    }
}
