package ba.hk.meetme.ui.gallery;

import org.immutables.value.Value;

import ba.hk.meetme.models.Feed;

@Value.Enclosing
public class GalleryViewModelEvents {

    @Value.Immutable
    public static abstract class ShowGalleryForFeed extends GalleryViewModelEvents {
        public abstract Feed feed();
    }
}
