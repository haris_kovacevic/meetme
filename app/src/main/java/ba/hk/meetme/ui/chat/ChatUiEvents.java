package ba.hk.meetme.ui.chat;

import org.immutables.value.Value;

@Value.Enclosing
public class ChatUiEvents {
    public static class Created extends ChatUiEvents {
    }

    @Value.Immutable
    public static abstract class SendMessageClicked extends ChatUiEvents {
        public abstract String text();
    }

}
