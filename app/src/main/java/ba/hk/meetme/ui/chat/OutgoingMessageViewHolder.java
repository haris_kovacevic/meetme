package ba.hk.meetme.ui.chat;


import android.support.v7.widget.RecyclerView;

import ba.hk.meetme.databinding.ItemOutgoingMessageBinding;
import ba.hk.meetme.models.Message;

public class OutgoingMessageViewHolder extends RecyclerView.ViewHolder {
    private final ItemOutgoingMessageBinding mBinding;

    public OutgoingMessageViewHolder(ItemOutgoingMessageBinding binding) {
        super(binding.getRoot());
        mBinding = binding;
    }

    public void bind(Message message) {
        mBinding.message.setText(message.text());
    }
}
