package ba.hk.meetme.ui.chat;


import android.arch.lifecycle.LiveDataReactiveStreams;
import android.arch.lifecycle.ViewModelProviders;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v7.widget.LinearLayoutManager;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.fernandocejas.arrow.strings.Strings;

import java.util.ArrayList;

import ba.hk.meetme.databinding.FragmentChatBinding;
import ba.hk.meetme.manager.ChatManager;
import ba.hk.meetme.models.Message;
import ba.hk.meetme.ui.BaseFragment;
import io.reactivex.Flowable;
import io.reactivex.flowables.ConnectableFlowable;

public class ChatFragment extends BaseFragment {
    private FragmentChatBinding mBinding;
    private ChatAdapter mAdapter;
    private ChatViewModel mViewModel;

    public static ChatFragment newInstance() {
        return new ChatFragment();
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        mBinding = FragmentChatBinding.inflate(inflater, container, false);
        return mBinding.getRoot();
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        setupUI();
        initViewModel();
        subscribeViewModel();
    }

    private void setupUI() {
        mAdapter = new ChatAdapter(new ArrayList<>());
        mBinding.messages.setLayoutManager(new LinearLayoutManager(getActivity()));
        mBinding.messages.setAdapter(mAdapter);
        mBinding.send.setOnClickListener(x -> {
            String text = mBinding.text.getText().toString();
            if (!TextUtils.isEmpty(text))
                mViewModel.postEvent(ImmutableChatUiEvents.SendMessageClicked.builder().text(text).build());
            mBinding.text.setText(Strings.EMPTY);
        });

    }

    private void initViewModel() {
        mViewModel = ViewModelProviders.of(this).get(ChatViewModel.class);
    }

    private void subscribeViewModel() {
        ConnectableFlowable<ChatUiModel> mUiModel = Flowable.fromPublisher(LiveDataReactiveStreams.toPublisher(this, mViewModel.getUiModel())).publish();
        mSubscriptions.addAll(
                ChatManager.getInstance().getMessageStream().distinctUntilChanged().subscribe(this::showMessage),
                ChatManager.getInstance().getMessageStream().firstElement().toObservable().subscribe(x -> hideLoading())
        );

        mViewModel.postEvent(new ChatUiEvents.Created());
        mUiModel.connect();
    }

    private void hideLoading() {
        mBinding.progress.setVisibility(View.GONE);
    }

    private void showMessage(Message message) {
        mAdapter.addMessage(message);
        mBinding.messages.smoothScrollToPosition(mAdapter.getItemCount() - 1);
    }
}
