package ba.hk.meetme.ui.feeds;

import com.yuyakaido.android.cardstackview.SwipeDirection;

import org.immutables.value.Value;

import ba.hk.meetme.models.Feed;

@Value.Immutable
public abstract class SwipedFeed {
    public abstract Feed feed();

    public abstract SwipeDirection direction();
}
