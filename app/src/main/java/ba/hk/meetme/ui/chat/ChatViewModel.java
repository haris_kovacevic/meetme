package ba.hk.meetme.ui.chat;


import android.arch.lifecycle.LiveData;
import android.arch.lifecycle.MutableLiveData;
import android.arch.lifecycle.ViewModel;

import ba.hk.meetme.manager.ChatManager;
import io.reactivex.BackpressureStrategy;
import io.reactivex.Flowable;
import io.reactivex.Observable;
import io.reactivex.disposables.Disposable;
import io.reactivex.subjects.PublishSubject;

public class ChatViewModel extends ViewModel {
    private PublishSubject<ChatUiEvents> mUiEvents = PublishSubject.create();
    private MutableLiveData<ChatUiModel> mUiModel = new MutableLiveData<>();
    private Disposable mSubscription;


    public ChatViewModel() {
        mSubscription = createStream().subscribe(uiModel -> mUiModel.postValue(uiModel));
    }

    private Flowable<ChatUiModel> createStream() {
        Observable<ChatViewModelEvents> events = Observable.merge(
                ChatManager.getInstance().getMessageStream().map(msg -> ImmutableChatViewModelEvents.ShowMessage.builder().message(msg).build()),
                mUiEvents.ofType(ChatUiEvents.Created.class).firstElement().toObservable().doOnNext(x -> ChatManager.getInstance().register()).map(x -> new ChatViewModelEvents()),
                mUiEvents.ofType(ChatUiEvents.SendMessageClicked.class).distinctUntilChanged().map(ev -> ev.text()).doOnNext(text -> ChatManager.getInstance().sendMessage(text)).map(x -> new ChatViewModelEvents())
        );
        ChatUiModel defaultUiModel = ImmutableChatUiModel.builder().build();
        Observable<ChatUiModel> uiModels = events.scan(defaultUiModel, (oldUiModel, event) -> {
            if (event instanceof ChatViewModelEvents.ShowMessage)
                return ImmutableChatUiModel.copyOf(oldUiModel).withShowMessage(((ChatViewModelEvents.ShowMessage) event).message());
            return oldUiModel;
        });
        return uiModels.toFlowable(BackpressureStrategy.LATEST);
    }

    public void postEvent(ChatUiEvents event) {
        mUiEvents.onNext(event);
    }


    @Override
    protected void onCleared() {
        super.onCleared();
        mSubscription.dispose();
    }

    public LiveData<ChatUiModel> getUiModel() {
        return mUiModel;
    }
}
