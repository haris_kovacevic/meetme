package ba.hk.meetme.ui.gallery;


import android.support.annotation.Nullable;

import com.fernandocejas.arrow.optional.Optional;

import org.immutables.value.Value;

import ba.hk.meetme.models.Feed;

@Value.Immutable
public abstract class GalleryUiModel {

    @Nullable
    public abstract Feed showGalleryForFeed();

    public Optional<Feed> showGalleryForFeedOpt() {
        return Optional.fromNullable(showGalleryForFeed());
    }
}
