package ba.hk.meetme.ui.gallery;


import android.arch.lifecycle.LiveData;
import android.arch.lifecycle.MutableLiveData;
import android.arch.lifecycle.ViewModel;

import io.reactivex.BackpressureStrategy;
import io.reactivex.Flowable;
import io.reactivex.Observable;
import io.reactivex.disposables.Disposable;
import io.reactivex.subjects.PublishSubject;

public class GalleryViewModel extends ViewModel {
    private PublishSubject<GalleryUiEvents> mUiEvents = PublishSubject.create();
    private MutableLiveData<GalleryUiModel> mUiModel = new MutableLiveData<>();
    private Disposable mSubscription;


    public GalleryViewModel() {
        mSubscription = createStream().subscribe(uiModel -> mUiModel.postValue(uiModel));
    }

    private Flowable<GalleryUiModel> createStream() {
        Observable<GalleryViewModelEvents> events = mUiEvents.ofType(GalleryUiEvents.Created.class).firstElement().toObservable().map(ev -> ImmutableGalleryViewModelEvents.ShowGalleryForFeed.builder().feed(ev.feed()).build());
        GalleryUiModel defaultUiModel = ImmutableGalleryUiModel.builder().build();
        Observable<GalleryUiModel> uiModels = events.scan(defaultUiModel, (oldModel, event) -> {
            if (event instanceof ImmutableGalleryViewModelEvents.ShowGalleryForFeed)
                return ImmutableGalleryUiModel.copyOf(oldModel).withShowGalleryForFeed(((ImmutableGalleryViewModelEvents.ShowGalleryForFeed) event).feed());
            return oldModel;
        });

        return uiModels.toFlowable(BackpressureStrategy.LATEST);

    }

    public void postEvent(GalleryUiEvents event) {
        mUiEvents.onNext(event);
    }


    @Override
    protected void onCleared() {
        super.onCleared();
        mSubscription.dispose();
    }

    public LiveData<GalleryUiModel> getUiModel() {
        return mUiModel;
    }
}
