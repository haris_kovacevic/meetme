package ba.hk.meetme.manager;

import com.google.firebase.database.ChildEventListener;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import ba.hk.meetme.api.DiskApi;
import ba.hk.meetme.models.ImmutableMessage;
import ba.hk.meetme.models.Message;
import io.reactivex.Observable;
import io.reactivex.subjects.PublishSubject;

public class ChatManager {
    private static final String REFERENCE_CHAT = "chat";
    private PublishSubject<Message> mMessageStream = PublishSubject.create();

    private static class Loader {
        static ChatManager INSTANCE = new ChatManager();
    }

    private ChatManager() {
        FirebaseDatabase.getInstance().setPersistenceEnabled(true);
    }

    public static ChatManager getInstance() {
        return Loader.INSTANCE;
    }

    public void sendMessage(String text) {
        sendMessage(getUser(), text);
    }

    public void sendMessage(String sender, String text) {
        FirebaseDatabase.getInstance().getReference(REFERENCE_CHAT).push().setValue(new ChatMessage(sender, text));
    }

    public void setUser(String email) {
        DiskApi.getInstance().persistUsername(email);
    }

    public String getUser() {
        return DiskApi.getInstance().readUsername();
    }


    public void register() {

        FirebaseDatabase.getInstance().getReference(REFERENCE_CHAT).addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                for (DataSnapshot chatMessageSnapshot : dataSnapshot.getChildren()) {
                    ChatMessage chatMessage = chatMessageSnapshot.getValue(ChatMessage.class);
                    if (chatMessage.sender != null)
                        mMessageStream.onNext(ImmutableMessage.builder()
                                .text(chatMessage.text)
                                .sender(chatMessage.sender)
                                .incoming(!chatMessage.sender.equals(getUser()))
                                .build());
                }
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });

        FirebaseDatabase.getInstance().getReference(REFERENCE_CHAT).addChildEventListener(new ChildEventListener() {
            @Override
            public void onChildAdded(DataSnapshot dataSnapshot, String s) {
                ChatMessage chatMessage = dataSnapshot.getValue(ChatMessage.class);
                if (chatMessage.sender != null)
                    mMessageStream.onNext(ImmutableMessage.builder()
                            .text(chatMessage.text)
                            .sender(chatMessage.sender)
                            .incoming(!chatMessage.sender.equals(getUser()))
                            .build());
            }

            @Override
            public void onChildChanged(DataSnapshot dataSnapshot, String s) {

            }

            @Override
            public void onChildRemoved(DataSnapshot dataSnapshot) {

            }

            @Override
            public void onChildMoved(DataSnapshot dataSnapshot, String s) {

            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });

    }

    public Observable<Message> getMessageStream() {
        return mMessageStream;
    }

    public static class ChatMessage {
        public String sender;
        public String text;

        public ChatMessage() {
        }

        public ChatMessage(String sender, String text) {
            this.sender = sender;
            this.text = text;
        }

        public String getSender() {
            return sender;
        }

        public void setSender(String sender) {
            this.sender = sender;
        }

        public String getText() {
            return text;
        }

        public void setText(String text) {
            this.text = text;
        }
    }

}
