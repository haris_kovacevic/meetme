package ba.hk.meetme.stores;


import android.util.Log;

import com.fernandocejas.arrow.optional.Optional;

import ba.hk.meetme.api.DiskApi;
import io.reactivex.Observable;
import io.reactivex.subjects.PublishSubject;

public class TokenStore {
    private static class Loader {
        private static TokenStore INSTANCE = new TokenStore();
    }

    public static TokenStore getInstance() {
        return Loader.INSTANCE;
    }

    private PublishSubject<Optional<String>> mStream = PublishSubject.create();

    private TokenStore() {
        mStream.onNext(Optional.fromNullable(DiskApi.getInstance().readToken()));
    }

    public void saveToken(String token) {
        DiskApi.getInstance().persistToken(token);
        mStream.onNext(Optional.fromNullable(token));
    }

    public Optional<String> get() {
        return Optional.fromNullable(DiskApi.getInstance().readToken());
    }

    public Observable<Optional<String>> stream() {
        return Observable.just(get()).concatWith(mStream).doOnNext(tokenOpt -> Log.d("Token store", "Token present: " + tokenOpt.isPresent()));
    }
}
